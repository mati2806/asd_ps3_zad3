﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3_PS3
{
    class Mercenary
    {
        public int strenght;
        public int costFood;
        public int costEntertainment;
        private static int counter=0;
        private int id = 0;
        public int cost = 0;

        public Mercenary(int strenght, int costFood, int costEntertainment)
        {
            this.strenght = strenght;
            this.costFood = costFood;
            this.costEntertainment = costEntertainment;
            cost = costFood * 1000 + costEntertainment;

            id = counter;
            counter++;

            
        }

        public int GetID()
        {
            return id;
        }
    }
}
