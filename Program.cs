﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;






namespace Zad3_PS3
{

    class Program
    {
        static int iterations=0;
        static int[,,] answers = new int[,,] { };
        
        static void Main(string[] args)
        {
            string fileName = "in6.txt";
            int mercenaryAmount = 0;      
            int resourceFood = 0;
            int resourceEntertainment = 0;
            int x, y, z;
            var watch = System.Diagnostics.Stopwatch.StartNew();


            List<Mercenary> mercenaries = new List<Mercenary>();


            try { LoadFile(fileName, ref mercenaryAmount, ref resourceFood, ref resourceEntertainment, mercenaries); }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie znaleziono pliku!");
                return;
            }

            answers = new int[mercenaryAmount, resourceFood+1, resourceEntertainment+1];

            Console.WriteLine("File: " + fileName);
            Console.WriteLine("|Resources|");
            Console.WriteLine("Food: " + resourceFood);
            Console.WriteLine("Entertainment: " + resourceEntertainment);
            Console.WriteLine("Mercenary Amount: " + mercenaryAmount);
            Console.WriteLine();

            //PrintMercenaryList(mercenaries);

            Console.WriteLine("Result: " + GetAnswer(mercenaries, resourceFood, resourceEntertainment, mercenaryAmount));
            Console.WriteLine("Iterations: " + iterations);
            watch.Stop();
            Console.WriteLine("Time: " + watch.ElapsedMilliseconds + " ms"); 
        }

        public static void LoadFile(string fileName, ref int mercenaryAmount, ref int resourceFood, ref int resourceEntertainment, List<Mercenary> merceneries)
        {
            int i = 0;
            int j = 0;
            string[] words = new string[] { };

            StreamReader reader = new StreamReader(fileName);

            words = reader.ReadToEnd().Split(new[] { '\n', ' ' });

            resourceFood = Convert.ToInt32(words[0]);
            resourceEntertainment = Convert.ToInt32(words[1]);
            mercenaryAmount = Convert.ToInt32(words[2]);

            for (i = 3; i < words.Length - 1; i += 3)
            {

                merceneries.Add(new Mercenary(Convert.ToInt32(words[i]), Convert.ToInt32(words[i + 1]), Convert.ToInt32(words[i + 2])));
                j++;
            }

        }

        public static void SaveFile(List<int> selected, int maxStrenght)
        {
            StreamWriter writer = new StreamWriter("out.txt");
            writer.WriteLine(maxStrenght);

            foreach(int i in selected)
            {
                writer.Write(i + " ");
            }
            writer.Close();
        }

        public static void PrintMercenaryList(List<Mercenary> mercenaries)
        {
            foreach (Mercenary m in mercenaries)
            {
                Console.Write(m.GetID() + ".S:" + m.strenght);
                Console.Write(" FC:" + m.costFood);
                Console.Write(" EC:" + m.costEntertainment);            
                Console.WriteLine();
            }
            Console.WriteLine();

        }


        public static int GetAnswer(List<Mercenary> mercenaries, int resourceFood, int resourceEntertainment, int mercenaryAmount)
        {
            int i, j, n;
            int maxStrenght = 0;
            List<int> selected = new List<int>();

            for (n = 0; n < mercenaryAmount; n++)
            {
                for (j = 0; j <= resourceFood; j++)
                {

                    for (i = 0; i <= resourceEntertainment; i++)
                    {
                        iterations++;

                        if (n == 0)
                        {
                            if (j >= mercenaries.ElementAt(n).costFood && i >= mercenaries.ElementAt(n).costEntertainment)
                            {
                                answers[n, j, i] = mercenaries.ElementAt(n).strenght;
                            }
                        }
                        else if (mercenaries.ElementAt(n).costFood > j || mercenaries.ElementAt(n).costEntertainment > i)
                        {
                            answers[n, j, i] = answers[n - 1, j, i];
                        }
                        else
                        {
                            answers[n, j, i] = Math.Max(answers[n - 1, j, i], answers[n - 1, j - mercenaries.ElementAt(n).costFood, i - mercenaries.ElementAt(n).costEntertainment] + mercenaries.ElementAt(n).strenght);
                        }

                            maxStrenght = answers[mercenaryAmount-1, resourceFood, resourceEntertainment];

                        /*
                        Console.Write(answers[n, j, i]);
                        if (answers[n, j, i] > 9) Console.Write(" ");
                        else Console.Write("  "); */


                    }
                    //Console.WriteLine();
                }
                //Console.WriteLine();
                //Console.WriteLine();
                //Console.WriteLine();
            }

            j = resourceFood;
            i = resourceEntertainment;

            

            for (n = mercenaryAmount-1; n >= 0; n--)
            {
                if(n>0)
                {
                    if (n > 0 && answers[n - 1, j, i] == answers[n, j, i])
                    {
                        continue;
                    }
                    else
                    {
                        selected.Add(n + 1);
                        j = j - mercenaries.ElementAt(n).costFood;
                        i = i - mercenaries.ElementAt(n).costEntertainment;
                    }
                }
                else
                {
                    if(answers[n, j, i] != 0)
                        selected.Add(n+1);
                }                            
            }

            selected.Sort();

            Console.Write("Selected: ");
            foreach (int merc in selected)
            {  
                Console.Write(merc + " ");
            }
            Console.WriteLine();

            SaveFile(selected, maxStrenght);

            return maxStrenght;

        }

    }
   
}


